# Load libraries and functions
source('functions.R')

# List .arw files and select ELS signal
elsd_sgn <- select.fl(fl_size = 200000)

# Read ELSD signal and combine all chromatogramas
elsd_mt <- foreach::foreach(i = elsd_sgn, .combine = "rbind") %do%
  read.arw(fl_name = i)
# Change raw names of chromatograms
rownames(elsd_mt) <- dirname(elsd_sgn) %>% 
  basename() %>% str_replace_all(pattern = " ", "_")

# cut the first 500 points
elsd_mt <- elsd_mt[, -seq(500)]

# Smoothing to enhance signal to noise ratio
elsd_smth <- matrix(nrow = nrow(elsd_mt), ncol = ncol(elsd_mt))
for (i in seq(nrow(elsd_mt))) {
  smoothed  <-  whit1(elsd_mt[i, ], lambda = 1e3/2)
  elsd_smth[i, ] <- smoothed
}

# Baseline correction
elsd_bsl <- baseline.corr(elsd_smth)


# Extracting info from file names
elsd_info <- elsd_sgn %>% lapply(get.info) %>% 
  lapply(as.data.frame) %>% lapply(t) %>% 
  lapply(as.data.frame) %>% bind_rows() %>% 
  mutate_all(factor) %>%
  mutate(Sample = tolower(Sample), N = 1:n()) %>% 
  unite(ID, Sample, Repetition, Fraction, Type, remove = F) %>% 
  mutate(ID = tolower(ID))

# Divide bacteria and culture media
bacteria_info <- elsd_info %>% filter(grepl("celula", ID))
media_info <- elsd_info %>% filter(!grepl("celula", ID))

# Parametric time warping alignment
bacteria_algn <- subset.alig(signal = elsd_bsl[bacteria_info$N,],
                             extra_vec = bacteria_info$N)
media_align <- subset.alig(signal = elsd_bsl[media_info$N,],
                           extra_vec = media_info$N)


### Gather metadata and chromatogramas
## Media
media_fn <- media_info
media_fn$Signal <- I(elsd_bsl[media_info$N,])
#media_fn$Signal <- I(media_align)

# PCA
autoplot(prcomp(media_fn$Signal, center = T, scale. = T),
         colour = "Sample", size = 2, data = media_fn) +
  facet_wrap("Fraction", scales = "free") + theme_bw()

## Bacteria
bacteria_fn <- bacteria_info
bacteria_fn$Signal <-I(elsd_bsl[bacteria_info$N, ])
#bacteria_fn$Signal <- I(bacteria_algn)

# PCA
autoplot(prcomp(bacteria_fn$Signal, center = T, scale. = T),
         colour = "Sample", size = 2, data = bacteria_fn) +
  facet_wrap("Fraction", scales = "free") + theme_bw()



# Other plots
plot(elsd_mt[1, ], type = 'l' ,ylim = c(00, 20))
lines(elsd_bsl[1, ], col = "blue")

plot(elsd_bsl[2, ], type = 'l')
lines(elsd_bsl[8,], col = "blue")
lines(elsd_bsl[14,], col = "red")
