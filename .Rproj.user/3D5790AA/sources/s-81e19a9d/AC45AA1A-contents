library(doParallel)
library(foreach)
library(tidyverse)
library(ptw)
library(ggfortify)

strsplit2 <- function(char, splt_chr) {
  rtn <- strsplit(x = char, split = splt_chr)[[1]]
  rtn
}

get.info <- function(fl_name) {
  injec_info <- strsplit2(fl_name, '/')[2]
  sample <- strsplit2(injec_info, ' ')[1]
  repetition <- strsplit2(injec_info, ' ')[2]
  fraction <- strsplit2(injec_info, ' ')[3]
  type <- strsplit2(injec_info, ' ')[4]
  info <- c(Sample = sample,Repetition = repetition,
            Fraction = fraction,Type = type)
  info
}

new.aligment <- function(data) {
  tmp_names <- rownames(data)
  data_ref <- data[2, ]
  data_sam <- data[-2, ]
  signal_align <- ptw(data_ref, data_sam, warp.type = "individual", 
                      verbose = TRUE, optim.crit = "RMS")
  aligned  <- signal_align$warped.sample
  aligned_f <- rbind(data_ref, aligned)
  aligned_f[is.na(aligned_f)] <- 0
  rownames(aligned_f) <- tolower(tmp_names)
  aligned_f
}

plot.loadings <- function(raw_data, lc_cut) {
  LC_pca <- prcomp(raw_data$Signal, center = T, scale. = T)
  
  LC_loadings <- LC_pca$rotation %>% dplyr::as_tibble() %>%
    dplyr::select('PC1', 'PC2', 'PC3') %>% 
    dplyr::mutate(Time = LC_time[-seq(1,lc_cut)]) %>%
    tidyr::gather(key = 'PCs', 'Value', PC1:PC3) %>% 
    dplyr::mutate(Kind = ifelse(Value > 0, 'Positivos', 'Negativos'),
           Min = floor(Time)) %>%
    dplyr::mutate(Kind = factor(Kind, levels = c('Positivos', 'Negativos')))
  
  loadings <- ggplot(LC_loadings, aes(factor(Min), Value, color = PCs)) +
    geom_boxplot() +
    #  geom_smooth()  +
    facet_grid(Kind~PCs,scales = 'free') + theme_bw()
  loadings
}

read.arw <- function(fl_name) {
  
  # Read row Data
  raw_data <- read_csv(fl_name, skip = 2,
                       col_names = F) %>% rename(Time = X1, Signal = X2)
  LC_time <<- raw_data$Time
  # Extract Channel 
  data_fn <- matrix(raw_data$Signal, nrow = 1)
  rownames(data_fn) <- dirname(fl_name) %>% 
    basename() %>% str_replace_all(pattern = " ", "_")
  
  # Gater all info into a single data frame
  data_fn
}

select.fl <- function(folder = 'data', fl_size = 6915853) {
  arw_fl <- list.files(folder, pattern = '.arw',
                       full.names = T, recursive = T)
  arw_ok <- sapply(arw_fl, file.size) < fl_size
  arw_fl <- arw_fl[arw_ok]
  arw_fl
}

subset.alig <- function(signal, extra_vec, each_id = 3) {
  uvalues <- seq(1, length(extra_vec)/each_id)
  index <- rep(uvalues, each = each_id)
  all_aligned <- NULL
  for (i in uvalues) {
    sample_sub <- signal[which(i == index), ]
    aligned_tmp <- new.aligment(sample_sub)
    if(i > 1) {
      all_aligned <- rbind(all_aligned, aligned_tmp)
    } else{
      all_aligned <- aligned_tmp
    }
  }
  all_aligned
}
